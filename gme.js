// document.querySelector('.bg').style.width=windowWidth
// document.querySelector('.bg').style.width=windowHeight
window.addEventListener("click", function () {
  document.querySelector(".gameStart").style =
    "display:none;pointers.event:none;";
  document.querySelector("#canvas").style = "display:flex";
  document.querySelector("#herosTonight").play();
});

// const UNIT_LENGTH = 12.5;
const unitLength = 12.5;
let columns = -1; /* To be determined by window width*/
let rows = -1; /* To be determined by window height */

const GAME_MODE = Object.freeze({
  NONE: 0,
  THE_FLOW: 1,
  NORMAL: 2,
});

// Array of Array<number>
let currentBoard = [];
let nextBoard = [];
let gameMode = GAME_MODE.NONE;

let speedC = document.querySelector("#myRange");
speedC.addEventListener("change", () => frameRate(parseInt(speedC.value)));

document
  .querySelector("#normal")
  .addEventListener("click", () => (gameMode = GAME_MODE.NORMAL));
document
  .querySelector("#theFlow")
  .addEventListener("click", () => (gameMode = GAME_MODE.THE_FLOW));

// KEYBOARD WORK
let xValue = 0;
let yValue = 0;

function setup() {
  /* Set the canvas to be under the element #canvas*/
  const canvas = createCanvas(windowWidth * 0.8, windowHeight * 0.9);
  canvas.parent(document.querySelector("#canvas"));

  /*Calculate the number of columns and rows */
  columns = floor(width / unitLength);
  rows = floor(height / unitLength);

  /*Making both currentBoard and nextBoard 2-dimensional matrix that has (columns * rows) boxes. */
  currentBoard = [];
  nextBoard = [];
  for (let i = 0; i < columns; i++) {
    currentBoard[i] = [];
    nextBoard[i] = [];
  }

  // Now both currentBoard and nextBoard are array of array of undefined values.
  init();
  console.log("currentBoard[0].length", currentBoard[0].length);
  console.log("currentBoard.length", currentBoard.length);

  // Set the initial values of the currentBoard and nextBoard
  getFrameRate(frameRate(parseInt(speedC.value)));
  frameRate(frameRate(parseInt(speedC.value)));
}

document.querySelector("#setup-circle").addEventListener("click", function () {
  document.querySelector("#song8").play();
  const graph = `
    .............OO.....OO.............
    .............OO.....OO.............
    ...................................
    ...................................
    ...................................
    ..............OO...OO..............
    .............O..O.O..O.............
    ............O...O.O...O............
    ................O.O................
    .............OOO...OOO.............
    ...................................
    ...................................
    .......O....OO.......OO....O.......
    OO....O..O..OO.......OO..O..O....OO
    OO...O...O...............O...O...OO
    .....O...O...............O...O.....
    ......OOO.................OOO......
    ...................................
    ......OOO.................OOO......
    .....O...O...............O...O.....
    OO...O...O...............O...O...OO
    OO....O..O..OO.......OO..O..O....OO
    .......O....OO.......OO....O.......
    ...................................
    ...................................
    .............OOO...OOO.............
    ................O.O................
    ............O...O.O...O............
    .............O..O.O..O.............
    ..............OO...OO..............
    ...................................
    ...................................
    ...................................
    .............OO.....OO.............
    .............OO.....OO.............`;

  const graphRows = graph.split("\n");
  console.log(graphRows);

  for (let i = 0; i < graphRows.length; i++) {
    let row = graphRows[i].split("");
    for (let j = 0; j < row.length; j++) {
      if (row[j] === "O") {
        currentBoard[i][j] = 1;
      } else if (row[j] === ".") {
        currentBoard[i][j] = 0;
      }
    }
  }
});

//randomly generator
let randomGenerator = document.querySelector("#random");
randomGenerator.addEventListener("click", function () {
  for (let i = 0; i < columns; i++) {
    for (let j = 0; j < rows; j++) {
      currentBoard[i][j] = Math.round(Math.random());
    }
  }
});

function init() {
  for (let i = 0; i < columns; i++) {
    for (let j = 0; j < rows; j++) {
      currentBoard[i][j] = 0;
      nextBoard[i][j] = 0;
    }
  }
}

function draw() {
  background(color("hsla(0,0%,0%,0.5)"));
  generate();

  for (let i = 0; i < columns; i++) {
    for (let j = 0; j < rows; j++) {
      if (currentBoard[i][j] === 1) {
        let random = Math.floor(Math.random() * 256);
        fill(color(`hsba(${random}, 40%, 100%, 0.9)`));
      } else {
        fill(color("hsla(51, 40%, 100%, 0.3)"));
      }
      stroke("rgb(235,135,235)");
      strokeWeight(3);
      rect(i * unitLength, j * unitLength, unitLength, unitLength);
    }
  }
}

function generate() {
  if (gameMode === GAME_MODE.NORMAL) {
    for (let x = 0; x < columns; x++) {
      for (let y = 0; y < rows; y++) {
        let neighbors = 0;
        for (let i of [-1, 0, 1]) {
          for (let j of [-1, 0, 1]) {
            if (i === 0 && j === 0) {
              continue;
            }
            neighbors +=
              currentBoard[(x + i + columns) % columns][(y + j + rows) % rows];
          }
        }

        // Rules of Life
        if (currentBoard[x][y] == 1 && neighbors < 2) {
          // Die of Loneliness
          nextBoard[x][y] = 0;
        } else if (currentBoard[x][y] == 1 && neighbors > 3) {
          // Die of Overpopulation
          nextBoard[x][y] = 0;
        } else if (currentBoard[x][y] == 0 && neighbors == 3) {
          // New life due to Reproduction
          nextBoard[x][y] = 1;
        } else {
          // Stasis
          nextBoard[x][y] = currentBoard[x][y];
        }
      }
    }
    [currentBoard, nextBoard] = [nextBoard, currentBoard];
  } else if (gameMode === GAME_MODE.THE_FLOW) {
    for (let x = 0; x < columns; x++) {
      for (let y = 0; y < rows; y++) {
        // Count all living members in the Moore neighborhood(8 boxes surrounding)
        let neighbors = 0;
        for (let i of [-1, 0]) {
          for (let j of [-1, 0]) {
            if (i === 0 && j === 0) {
              // the cell itself is not its own neighbor
              continue;
            }
            // The modulo operator is crucial for wrapping on the edge
            neighbors +=
              currentBoard[(x + i + columns) % columns][(y + j + rows) % rows];
          }
        }
        // Rules of Life
        if (currentBoard[x][y] == 1 && neighbors == 0) {
          // Die of Loneliness
          nextBoard[x][y] = 0;
        } else if (currentBoard[x][y] == 0 && neighbors == 1) {
          // New life due to Reproduction
          nextBoard[x][y] = 1;
        } else {
          // Stasis
          nextBoard[x][y] = currentBoard[x][y];
        }
      }
    }
    [currentBoard, nextBoard] = [nextBoard, currentBoard];
  }
  //Loop over every single box on the board
  // Swap the nextBoard to be the current Board
}

/**
 * When mouse is dragged
 */

function mouseDragged() {
  /**
   * If the mouse coordinate is outside the board
   */
  if (mouseX > unitLength * columns || mouseY > unitLength * rows) {
    return;
  }
  const x = Math.floor(mouseX / unitLength);
  const y = Math.floor(mouseY / unitLength);
  currentBoard[x][y] = 1;
  fill(color("hsla(300,60%,70%,0.7)"));
  stroke(color("hsla(300,60%,70%,0.3)"));
  square(x * unitLength, y * unitLength, unitLength, unitLength);
  document.querySelector("#song5").play();
}
/**
 * When mouse is pressed
 */
function mousePressed() {
  noLoop();
  mouseDragged();
}

/**
 * When mouse is released
 */
function mouseReleased() {
  loop();
  document.querySelector("#song7").play();
}

document.querySelector("#reset-game").addEventListener("click", function () {
  init();
  setup();
});

// Press function
// window.addEventListener("keydown", function () {});

window.addEventListener("keydown", function (e) {
  let isKeyArrow = false;
  let songID = "#song1";
  if (e.key === "ArrowRight") {
    isKeyArrow = true;
    //must be less than x length
    xValue = (xValue + 1 + currentBoard.length) % currentBoard.length;
  } else if (e.key === "ArrowLeft") {
    console.log("ArrowLeft");
    isKeyArrow = true;
    songID = "#song2";
    console.log(xValue);
    xValue = (xValue - 1 + currentBoard.length) % currentBoard.length;
  } else if (e.key === "ArrowDown") {
    isKeyArrow = true;
    songID = "#song3";
    yValue = (yValue + 1 + currentBoard[0].length) % currentBoard[0].length;
  } else if (e.key === "ArrowUp") {
    isKeyArrow = true;
    songID = "#song4";
    yValue = (yValue - 1 + currentBoard[0].length) % currentBoard[0].length;
  }
  // if (e.key.startsWith("Arrow")) {
  //     fill(color("hsla(300,60%,70%,0.7)"));
  //     stroke(color("hsla(300,60%,70%,0.3)"));
  // }
  if (isKeyArrow) {
    document.querySelector(songID).play();
    console.log(xValue, yValue);
    currentBoard[xValue][yValue] = 1;
    fill(color("hsla(300,60%,70%,0.7)"));
    stroke(color("hsla(300,60%,70%,0.3)"));
  }
});

function windowResized() {
  setup();
}

// function playingMusicBasedOnI(i, j) {
//   for (let i = 0; i < columns; i++) {
//     if (currentBoard[i][j] === 1) {
//       if ((i + 4) % 4 === 0) {
//         theSong = "#boardXSong1";
//       } else if ((i + 4) % 4 === 1) {
//         theSong = "#boardXSong2";
//       } else if ((i + 4) % 4 === 2) {
//         theSong = "#boardXSong3";
//       } else if ((i + 4) % 4 === 3) {
//         theSong = "#boardXSong4";
//       }
//     }
//     document.querySelector(theSong).play();
//   }
// }

document.querySelector(".stop").addEventListener("click", () => noLoop());

document
  .querySelector(".mute")
  .addEventListener(
    "click",
    () => (document.querySelector("#herostonight").muted = true)
  );
